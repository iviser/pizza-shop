<?php

namespace App\Utilities;

use App\Models\Ingredient;
use App\Models\Order;
use App\Models\Recipe;
use App\Utilities\Oven\Oven;
use Illuminate\Support\Collection;

class Luigis
{
    private Fridge $fridge;
    private Oven $oven;

    public function __construct(Fridge $fridge, Oven $oven)
    {
        $this->oven = $oven;
        $this->fridge = $fridge;
    }

    public function restockFridge(): void
    {
        /** @var Ingredient $ingredient */
        foreach (Ingredient::all() as $ingredient) {
            $this->fridge->add($ingredient, 10);
        }
    }

    /**
     * @param Order $order
     * @return Pizza[]|Collection
     */
    public function deliver(Order $order): Collection
    {
        $this->oven->heatUp();
        $order->status = Order::STATUS_PREPARING;

        $pizzas = $order->recipes->map(function (Recipe $recipe) {
            $pizza = $this->prepare($recipe);
            $this->cook($pizza);

            return $pizza;
        });

        $this->oven->turnOff();
        $order->status = Order::STATUS_READY;

        return $pizzas;
    }

    private function prepare(Recipe $recipe): Pizza
    {
        // 1) Check fridge has enough of each ingredient
        if (!$this->fridgeHasEnoughIngredients($recipe)) {
            // 2) restockFridge if needed
            $this->restockFridge();
        };

        // 3) take ingredients from the fridge
        foreach ($recipe->ingredientRequirements as $ingredientRequirement) {
            $this->fridge->take($ingredientRequirement->ingredient, $ingredientRequirement->amount);
        }

        // 4) create new Pizza
        return new Pizza($recipe);
    }

    private function cook(Pizza $pizza): void
    {
        $this->oven->bake($pizza);
    }

    private function fridgeHasEnoughIngredients(Recipe $recipe): bool
    {
        foreach ($recipe->ingredientRequirements as $ingredient) {
            if (!$this->fridge->has($ingredient->ingredient, $ingredient->amount)) {
                return false;
            }
        }

        return true;
    }
}
