<?php

declare(strict_types=1);


namespace App\Utilities\Oven;


use App\Models\Ingredient;
use App\Utilities\Pizza;
use DomainException;

class ElectricOven implements Oven
{
    private const MINIMAL_BAKE_TIME_PER_PIZZA = 5;
    private const DEFAULT_INCREMENT_PER_INGREDIENT_TYPE = 1;

    /**
     * @inheritdoc
     */
    public function heatUp(): Oven
    {
        $this->log("10 minutes to heat up");

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function bake(Pizza $pizza): Oven
    {
        $ingredients = $pizza->getRecipe()
            ->ingredients
            ->unique('id');

        $minutes = array_reduce(
            $ingredients->all(),
            fn(int $carry, Ingredient $ingredient) => $carry + self::DEFAULT_INCREMENT_PER_INGREDIENT_TYPE,
            self::MINIMAL_BAKE_TIME_PER_PIZZA
        );

        $this->updatePizzaCondition($pizza);

        $this->log(sprintf("%u minutes to bake pizza" ,$minutes));

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function turnOff(): Oven
    {
        $this->log('oven is off');
        return $this;
    }

    private function log(string $message): void
    {
        echo $message . PHP_EOL;
    }

    private function updatePizzaCondition(Pizza $pizza): void
    {
        if ($pizza->getStatus() === Pizza::STATUS_RAW) {
            $pizza->setStatus(Pizza::STATUS_COOKED);
            return;
        }

        if ($pizza->getStatus() === Pizza::STATUS_COOKED) {
            $pizza->setStatus(Pizza::STATUS_OVER_COOKED);
            return;
        }

        $correctStatusesStr = implode(', ', [Pizza::STATUS_RAW, Pizza::STATUS_COOKED]);

        throw new DomainException(
            sprintf(
                'Expected pizza status is one of [%s]. %s status given',
                $correctStatusesStr, $pizza->getStatus()
            )
        );
    }

}
