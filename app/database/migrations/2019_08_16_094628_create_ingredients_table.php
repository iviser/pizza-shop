<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIngredientsTable extends Migration {

	public function up(): void
	{
		Schema::create('luigis_ingredients', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name', 255);
		});
	}

	public function down(): void
	{
		Schema::drop('luigis_ingredients');
	}
}
