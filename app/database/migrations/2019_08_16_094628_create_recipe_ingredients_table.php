<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRecipeIngredientsTable extends Migration {

	public function up(): void
	{
		Schema::create('luigis_recipe_ingredients', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('recipe_id')->unsigned();
			$table->integer('ingredient_id')->unsigned();
			$table->integer('amount')->unsigned();
		});
	}

	public function down(): void
	{
		Schema::drop('luigis_recipe_ingredients');
	}
}
