<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        Model::unguard();

        $this->call(IngredientTableSeeder::class);
        $this->command->info('Ingredient table seeded!');

        $this->call(RecipeTableSeeder::class);
        $this->command->info('Recipe table seeded!');

        $this->call(RecipeIngredientTableSeeder::class);
        $this->command->info('RecipeIngredient table seeded!');
    }
}
