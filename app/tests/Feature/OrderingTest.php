<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Models\Order;
use App\Models\OrderRecipe;
use App\Models\Recipe;
use App\Utilities\Fridge;
use App\Utilities\Luigis;
use App\Utilities\Oven\ElectricOven;
use App\Utilities\Pizza;
use BadFunctionCallException;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

final class OrderingTest extends TestCase
{
    use DatabaseTransactions;

    private Luigis $luigis;

    protected function setUp(): void
    {
        parent::setUp();
        $this->luigis = new Luigis(
            new Fridge(),
            new ElectricOven()
        );
    }

    public function testCreateOrderWithMultipleRecipes(): Order
    {
        $recipes = Recipe::query()->whereIn('id', [Recipe::MARGHERITA_ID, Recipe::HAWAIIAN_ID])->get();

        return $this->createOrderWithRecipes($recipes);
    }

    /**
     * @depends testCreateOrderWithMultipleRecipes
     */
    public function testDeliverOrder(Order $order)
    {
        $pizzas = $this->luigis->deliver($order);

        $this->assertEquals($order->recipes->count(), count($pizzas));
        $recipesNames = $order->recipes->pluck('name');

        foreach ($pizzas as $pizza) {
            $this->assertContains($pizza->getName(), $recipesNames);
            $this->assertEquals(Pizza::STATUS_COOKED, $pizza->getStatus());
        }

        return $pizzas;
    }

    /**
     * @depends testDeliverOrder
     */
    public function testMargherita(Collection $pizzas): void
    {
        foreach ($pizzas as $pizza) {
            $this->eatSlice($pizza);
            $this->verifyCantEatEatenPizza($pizza);
        }
    }

    /**
     * @param Collection|Recipe[] $recipes
     * @return Order
     */
    protected function createOrderWithRecipes(Collection $recipes): Order
    {
        $order = Order::create(['status' => Order::STATUS_PENDING]);

        foreach ($recipes as $recipe) {
            OrderRecipe::create([
                'order_id' => $order->id,
                'recipe_id' => $recipe->id,
            ]);
        }

        $orderRecipesIds = $order->refresh()->recipes->pluck('id');
        $recipesIds = $recipes->pluck('id');

        $this->assertTrue($recipesIds->diff($orderRecipesIds)->isEmpty());
        $this->assertEquals($recipes->count(), $order->recipes->count());
        $this->assertEquals(
            $recipes->reduce(fn(float $carry, Recipe $recipe) => $carry + $recipe->price, 0.0),
            $order->getPriceAttribute()
        );

        return $order;
    }

    protected function eatSlice(Pizza $pizza): void
    {
        $pizza->eatSlice();

        $this->assertEquals(7, $pizza->getSlicesRemaining());
        $this->assertEquals(Pizza::STATUS_PARTLY_EATEN, $pizza->getStatus());

        while ($pizza->getSlicesRemaining()) {
            $pizza->eatSlice();
        }
        $this->assertEquals(Pizza::STATUS_ALL_EATEN, $pizza->getStatus());
    }

    protected function verifyCantEatEatenPizza(Pizza $pizza): void
    {
        $gotException = 'no exception thrown';
        try {
            $pizza->eatSlice();
        } catch (BadFunctionCallException $e) {
            $gotException = 'exception was thrown';
        }
        $this->assertEquals('exception was thrown', $gotException);
    }
}
