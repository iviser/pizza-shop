DK = docker
DKC = docker-compose -f docker-compose.yml
DKCL = ${DKC} -f docker-compose.local.yml

SVC_BE = php
SVC_DB = db
SVC_WEBSERVER = nginx
SVC_ES = elasticsearch
SVC_KIBANA = kibana
SVC_ADMINER = adminer

DEV_SVCS = ${SVC_BE} ${SVC_DB} ${SVC_WEBSERVER} ${SVC_ADMINER}

JOB_BE_INSTALL = composer-install

info:
	@echo 'Hello! To start using make, please read the README.md'

up: install
	${DKCL} up -d ${DEV_SVCS} && ${DKCL} ps

stop:
	${DKCL} stop ${DEV_SVCS}

down:
	${DKCL} down 

logs:
	${DKCL} logs

clean:
	rm -rf ./app/vendor

refresh: down clean up db-refresh

db-refresh:
	./artisan migrate:refresh && ./artisan db:seed

test-all:
	./workspace ./vendor/bin/phpunit

re-build: down
	${DKCL} build --no-cache

be-install:
	${DKCL} run --rm ${JOB_BE_INSTALL}

install: be-install
