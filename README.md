# Pizza shop
## Get started
### Run linux local environment 
1. Clone repository
2. Verify Your local environment variables. Make sure Nginx (:80) and DB (:5432) ports are not busy.  
   Use `env_example` for these purposes. Just copy the file and rename to `.env`.
3. run `make up` command

### How to refresh DB (Recreate db tables and run seeds)
Run `make db-refresh`. Make sure You started containers previously 

### Development scripts
 - Use `./artisan` script to run artisan commands within the php container
 - Use `./workspace` script to run any commands within the php container

### Run tests
Run `make test-all`

## Upcoming features
- [ ] People want to add or remove ingredients from pizzas in their order
    - Removing an ingredient from an existing recipe does not change the base price
    - Each new ingredient costs money
    - Ingredients can have different prices
    - Example: Mozzarella costs 50p, and person adds two extra orders of Mozzarella. Pizza price increases by £1

- [ ] People want to make custom pizzas
    - Implement the Recipe to allow for this
    - Same ingredient cost rules apply here
- [ ] integrate PHPStan (static code analyzer)  

## Known issues
1. Excessive DB query. Need to be improved with eloquent models relations pre-load
2. DB queries appear during service initialization see `\App\Utilities\Fridge`
3. Lack of test coverage. Need to write Functional tests and Unit tests